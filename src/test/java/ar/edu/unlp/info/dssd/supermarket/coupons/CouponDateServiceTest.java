package ar.edu.unlp.info.dssd.supermarket.coupons;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;
import ar.edu.unlp.info.dssd.supermarket.coupons.services.CouponDateServiceImpl;

public class CouponDateServiceTest extends IntegrationTest {

	@Autowired
	CouponDateServiceImpl service;

	@Test
	public void testCreateCouponDate() {
		CouponDate date = new CouponDate();
		date.setDiscountPercent(0.1D);
		date.setFrom(new Date());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 20);

		date.setTo(cal.getTime());

		service.create(date);
	}

}
