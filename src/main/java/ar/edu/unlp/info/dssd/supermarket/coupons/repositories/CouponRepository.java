package ar.edu.unlp.info.dssd.supermarket.coupons.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.Coupon;

@Repository
public interface CouponRepository extends PagingAndSortingRepository<Coupon, Long> {

	List<Coupon> findByNumber(Integer number);

}
