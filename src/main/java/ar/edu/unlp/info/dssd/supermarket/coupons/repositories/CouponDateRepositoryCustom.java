package ar.edu.unlp.info.dssd.supermarket.coupons.repositories;

import java.util.Date;
import java.util.List;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;

public interface CouponDateRepositoryCustom {

	List<CouponDate> findCouponDateForDate(Date aDate);
	
}
