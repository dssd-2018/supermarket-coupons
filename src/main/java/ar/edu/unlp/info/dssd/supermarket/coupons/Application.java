package ar.edu.unlp.info.dssd.supermarket.coupons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("ar.edu.unlp.info.dssd.supermarket.coupons")
@EntityScan(basePackages = { Application.DOMAIN_PACKAGE })
@EnableJpaRepositories(basePackages = { "ar.edu.unlp.info.dssd.supermarket.coupons.repositories" })
public class Application extends SpringBootServletInitializer {

	public static final String DOMAIN_PACKAGE = "ar.edu.unlp.info.dssd.supermarket.coupons.domain";

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
