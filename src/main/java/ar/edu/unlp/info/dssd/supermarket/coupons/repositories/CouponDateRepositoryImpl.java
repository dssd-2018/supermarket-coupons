package ar.edu.unlp.info.dssd.supermarket.coupons.repositories;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;

@Repository
public class CouponDateRepositoryImpl implements CouponDateRepositoryCustom {

	@Autowired
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<CouponDate> findCouponDateForDate(Date aDate) {

		String select = "select cd ";
		String from = "from CouponDate cd ";
		String where = "where cd.from <= :a_date and cd.to > :a_date ";
		String orderBy = "order by cd.id desc";

		Query query = entityManager.createQuery(select + from + where + orderBy).setParameter("a_date", aDate,
				TemporalType.TIMESTAMP);
		return query.getResultList();
	}

}
