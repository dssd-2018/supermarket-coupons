package ar.edu.unlp.info.dssd.supermarket.coupons.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "coupon_date")
public class CouponDate {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "from_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty(required = true)
	private Date from;
	
	@Column(name = "to_date")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty(required = true)
	private Date to;
	
	@Column(name = "discount_percent")
	@JsonProperty(required = true)
	private Double discountPercent;

	public Long getId() {
		return this.id;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}
	
}
