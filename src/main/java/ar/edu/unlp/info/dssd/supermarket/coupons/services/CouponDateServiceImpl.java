package ar.edu.unlp.info.dssd.supermarket.coupons.services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;
import ar.edu.unlp.info.dssd.supermarket.coupons.repositories.CouponDateRepository;

@Service
@Transactional
public class CouponDateServiceImpl {

	@Autowired
	CouponDateRepository repo;

	public CouponDate read(Long id) {
		CouponDate findOne = repo.findOne(id);
		if (findOne == null)
			throw new NotFoundException("Coupons date not found.");
		return findOne;
	}

	public Iterable<CouponDate> findAll() {
		return repo.findAll();
	}

	public Long create(CouponDate coupon) {
		this.validateCouponDate(coupon);
		return repo.save(coupon).getId();
	}

	public void delete(Long id) {
		repo.delete(id);
	}

	@Transactional
	public CouponDate update(CouponDate aCoupon) {
		this.validateCouponDate(aCoupon);
		CouponDate findOne = repo.findOne(aCoupon.getId());
		if (findOne == null)
			throw new NotFoundException("Date coupon no encontrado");
		findOne.setFrom(aCoupon.getFrom());
		findOne.setTo(aCoupon.getTo());
		findOne.setDiscountPercent(aCoupon.getDiscountPercent());
		return aCoupon;
	}

	public void validateCouponDate(CouponDate object) {

		if (object.getFrom().after(object.getTo()))
			throw new WebApplicationException("From date is greater than to date.", 409);
		if (object.getDiscountPercent() > 1D)
			throw new WebApplicationException("Discount cannot be greater than 1 (100%)", 409);
		if (object.getDiscountPercent() < 0)
			throw new WebApplicationException("Discount cannot be negative.", 409);

	}
	
	public List<CouponDate> findCouponDateForDate(Date date) {
		List<CouponDate> findCouponDateForDate = repo.findCouponDateForDate(date);
		return findCouponDateForDate;
	}

}
