package ar.edu.unlp.info.dssd.supermarket.coupons.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.ApiInfo;

@Service
@PropertySource("classpath:application.properties")
public class ApiInfoServiceImpl {

	@Value("${api.version}")
	String apiVersion = "0";


	@Transactional
	public ApiInfo getInfo() {
		ApiInfo info = new ApiInfo();

		info.version = apiVersion;
		info.sha = System.getenv("COMMIT_SHA");
		info.deploy_timestamp = System.getenv("LAST_DEPLOY_TIMESTAMP");
		info.status = this.testDatabaseStatus();
		return info;
	}

	private String testDatabaseStatus() {
	/*	try {
			productTypeRepository.count();
		} catch (Exception e) {
			return "fail";
		}
		*/
		return "ok";
	}

}
