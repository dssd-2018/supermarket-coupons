package ar.edu.unlp.info.dssd.supermarket.coupons.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.Coupon;
import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;
import ar.edu.unlp.info.dssd.supermarket.coupons.services.CouponDateServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.coupons.services.CouponServiceImpl;

@Component
@Singleton
@Path("api")
public class Api {

	@Autowired
	CouponServiceImpl couponService;

	@Autowired
	CouponDateServiceImpl couponDateService;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupons")
	public Response coupons(@QueryParam("number") Integer number) {
		if (number == null)
			return Response.ok(couponService.findAll()).build();
		return Response.ok(couponService.findByNumber(number)).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupon/{id}")
	public Response coupons(@PathParam("id") Long id) {
		return Response.ok(couponService.read(id)).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupon")
	public Response create(Coupon coupon) throws URISyntaxException {
		return Response.created(new URI("api/coupon/" + couponService.create(coupon).toString())).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupon")
	public Response update(Coupon acoupon) {
		return Response.ok(couponService.update(acoupon)).build();
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupon/{id}")
	public Response deletecoupon(@PathParam("id") long id) {
		couponService.delete(id);
		return Response.ok().build();
	}

	@POST
	@Path("coupondate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crateCouponDate(CouponDate object) throws URISyntaxException {
		return Response.created(new URI("api/coupondate/" + couponDateService.create(object).toString())).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupondates")
	public Response couponDates(@QueryParam("date") String date) {
		if (date == null)
			return Response.ok(couponDateService.findAll()).build();
		
		try {
			SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
			return Response.ok(couponDateService.findCouponDateForDate(parser.parse(date))).build();
		} catch (ParseException e) {
			throw new BadRequestException("Date not valid: "+date+" Should be in dd-MM-yyyy format.");
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupondate/{id}")
	public Response couponDate(@PathParam("id") Long id) {
		return Response.ok(couponDateService.read(id)).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("coupondate")
	public Response updateCouponDate(CouponDate acoupon) {
		return Response.ok(couponDateService.update(acoupon)).build();
	}

	@DELETE
	@Path("coupondate/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletecoupondate(@PathParam("id") long id) {
		couponDateService.delete(id);
		return Response.ok().build();
	}
}
