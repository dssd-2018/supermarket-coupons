package ar.edu.unlp.info.dssd.supermarket.coupons.services;

import java.util.List;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.Coupon;
import ar.edu.unlp.info.dssd.supermarket.coupons.repositories.CouponRepository;

@Service
@Transactional
public class CouponServiceImpl {

    @Autowired
    CouponRepository CouponRepository;

    public Coupon read(Long id) {
        Coupon findOne = CouponRepository.findOne(id);
        if (findOne == null)
            throw new NotFoundException("Coupon not found.");
        return findOne;
    }

    public Iterable<Coupon> findAll() {
        return CouponRepository.findAll();
    }

    public Long create(Coupon coupon) {
        List<Coupon> list = findByNumber(coupon.getNumber());
        if (!list.isEmpty())
            throw new WebApplicationException("Coupon already exists.", 409);
        return CouponRepository.save(coupon).getId();
    }

    public void delete(Long id) {
        CouponRepository.delete(id);
    }

    @Transactional
    public Coupon update(Coupon aCoupon) {
        Coupon findOne = CouponRepository.findOne(aCoupon.getId());
        if (findOne == null)
            throw new NotFoundException("Coupon no encontrado");
        findOne.setNumber(aCoupon.getNumber());
        findOne.setUsed(aCoupon.getUsed());
        return aCoupon;
    }

    public List<Coupon> findByNumber(Integer number) {
        return CouponRepository.findByNumber(number);
    }

}
