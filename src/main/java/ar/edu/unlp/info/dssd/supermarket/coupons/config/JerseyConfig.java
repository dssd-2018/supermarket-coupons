package ar.edu.unlp.info.dssd.supermarket.coupons.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import ar.edu.unlp.info.dssd.supermarket.coupons.api.Api;
import ar.edu.unlp.info.dssd.supermarket.coupons.api.ApplicationExceptionMapper;
import ar.edu.unlp.info.dssd.supermarket.coupons.api.CorsFilter;
import ar.edu.unlp.info.dssd.supermarket.coupons.api.GenericExceptionMapper;

@Configuration
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		registerEndpoints();
	}

	private void registerEndpoints() {
		// register(...);
		packages(Api.class.getClass().getPackage().getName());
		register(Api.class);
		register(GenericExceptionMapper.class);
		register(ApplicationExceptionMapper.class);
		register(CorsFilter.class);
	}
}
