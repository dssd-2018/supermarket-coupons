package ar.edu.unlp.info.dssd.supermarket.coupons.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.info.dssd.supermarket.coupons.domain.CouponDate;

@Repository
public interface CouponDateRepository extends PagingAndSortingRepository<CouponDate, Long>, CouponDateRepositoryCustom {

}
